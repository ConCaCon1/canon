import {
  ADD_SEARCH_HISTORY,
  REMOVE_SEARCH_HISTORY,
} from '../actions/searchHistoryActions';

const initialState = {
  history: [],
};

const searchHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SEARCH_HISTORY:
      return {
        ...state,
        history: [...state.history, action.payload],
      };
    case REMOVE_SEARCH_HISTORY:
      return {
        ...state,
        history: state.history.filter((query) => query !== action.payload),
      };
    default:
      return state;
  }
};

export default searchHistoryReducer;
