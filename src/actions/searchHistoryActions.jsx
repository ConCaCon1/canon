export const ADD_SEARCH_HISTORY = 'ADD_SEARCH_HISTORY';
export const REMOVE_SEARCH_HISTORY = 'REMOVE_SEARCH_HISTORY';
export const addSearchHistory = (query) => ({
  type: ADD_SEARCH_HISTORY,
  payload: query,
});
export const removeSearchHistory = (query) => ({
  type: REMOVE_SEARCH_HISTORY,
  payload: query,
});

// import { createSlice } from '@reduxjs/toolkit';

// const searchHistorySlice = createSlice({
//   name: 'searchHistory',
//   initialState: [],
//   reducers: {
//     addHistory: (state, action) => {
//       const updatedHistory = [action.payload, ...state.filter(item => item !== action.payload)];
//       localStorage.setItem('searchHistory', JSON.stringify(updatedHistory));
//       return updatedHistory;
//     },
//     removeHistory: (state, action) => {
//       const updatedHistory = state.filter(item => item !== action.payload);
//       localStorage.setItem('searchHistory', JSON.stringify(updatedHistory));
//       return updatedHistory;
//     },
//     loadHistory: (state, action) => {
//       return action.payload || [];
//     }
//   }
// });